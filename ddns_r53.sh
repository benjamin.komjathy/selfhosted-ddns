set -o allexport        
source .env
set +o allexport

PUB_IP=$(curl checkip.amazonaws.com)
HOSTED_ZONE=$HOSTED_ZONE

while read ROW; do
  echo "$ROW"

JSON='{
    "Changes": [
        {
            "Action": "UPSERT",
            "ResourceRecordSet": {
                "Name": "'$ROW'",
                "ResourceRecords": [
                    {
                        "Value": "'$PUB_IP'"
                    }
                ],
                "TTL": 60,
                "Type": "A"
            }
        }
    ],
    "Comment": "Web Server"
}'
echo $JSON > $PWD/ddns.json
aws route53 change-resource-record-sets --profile $AWS_PROFILE --hosted-zone-id $HOSTED_ZONE --change-batch file://ddns.json >> $PWD/ddns.logs
done <input.txt